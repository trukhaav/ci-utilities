#!/bin/bash

set -e

readonly git_url='https://gitlab.kitware.com/utils/ghostflow-director.git'
readonly git_commit='bb67a2502eb389a4604ada2b16ac60184c20d848' # bypass token fix

git clone "$git_url" ghostflow-director/src
pushd ghostflow-director/src
git -c advice.detachedHead=false checkout "$git_commit"
short_commit="$( git rev-parse --short "$git_commit" )"
readonly short_commit
cargo build --features systemd
popd
mv ghostflow-director/src/target/debug/ghostflow-director "ghostflow-director-$short_commit"
