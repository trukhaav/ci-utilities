readonly git_url='https://github.com/mozilla/sccache.git'
readonly git_commit='6b6d2f7d2dceefeb4f583712aa4c221db62be0bd' # v0.2.15
readonly version='0.2.15-background-init'

git clone "$git_url" sccache/src
cd sccache/src
git -c advice.detachedHead=false checkout "$git_commit"
git config user.name 'user'
git config user.email 'user@localhost'
git am ../*.patch
