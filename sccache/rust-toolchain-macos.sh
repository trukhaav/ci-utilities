# Tell rustup and cargo to stay within our work tree.
export RUSTUP_HOME=$GIT_CLONE_PATH/rust/rustup
export CARGO_HOME=$GIT_CLONE_PATH/rust/cargo

# Install a rust toolchain.
curl https://sh.rustup.rs -sSf | sh -s -- --no-modify-path --default-toolchain ${1} -y
source $CARGO_HOME/env

# Add support for needed architectures.
rustup target add x86_64-apple-darwin
rustup target add aarch64-apple-darwin

# Report result for human reference.
rustc --version
