#!/bin/sh

set -e

. sccache/package-common.sh

echo "Building for x86_64-apple-darwin"
env \
  MACOSX_DEPLOYMENT_TARGET=10.12 \
  SDKROOT="$DEVELOPER_DIR/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk" \
  cargo build --no-default-features --features=redis --release --target x86_64-apple-darwin

echo "Building for aarch64-apple-darwin"
env \
  MACOSX_DEPLOYMENT_TARGET=11.0 \
  SDKROOT="$DEVELOPER_DIR/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk" \
  cargo build --no-default-features --features=redis --release --target aarch64-apple-darwin

echo "Combining into universal binary"
out="$GIT_CLONE_PATH/sccache-v$version-universal-apple-darwin"
lipo -create target/*-apple-darwin/release/sccache -output "$out"
chmod +x "$out"
