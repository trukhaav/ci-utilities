#!/bin/sh

set -e

. cargo2junit/package-common.sh

echo "Building for x86_64-unknown-linux-gnu"
cargo build --release --target x86_64-unknown-linux-gnu
out="$GIT_CLONE_PATH/cargo2junit-v$version-x86_64-unknown-linux-gnu"
cp target/x86_64-unknown-linux-gnu/release/cargo2junit "$out"
x86_64-linux-gnu-strip "$out"
