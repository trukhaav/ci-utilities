#!/bin/sh

# Run this script on a macOS host to generate Qt binaries.

set -e
set -x

umask 022

readonly short_version="5.15"
readonly patch_version="2"
readonly version="$short_version.$patch_version"
readonly filename="qt-everywhere-src-$version.tar.xz"
readonly sha256sum="3a530d1b243b5dec00bc54937455471aaa3e56849d2593edb8ded07228202240"
readonly macos_target="11.0"

# Download, verify, and extract sources.
curl -OL "https://download.qt.io/archive/qt/$short_version/$version/single/$filename"
echo "$sha256sum  $filename" > qt.sha256sum
shasum -a 256 --check qt.sha256sum
tar xJf "$filename"

patch -p1 -d "qt-everywhere-src-$version/qtbase" < qt/qtbase-xcode13-fix.patch

readonly nproc="$( sysctl -n hw.ncpu )"
readonly here="$( pwd )"

# Build the arm64 variant.
mkdir "qt-$version-arm64"
cd "qt-$version-arm64"
"../qt-everywhere-src-$version/configure" \
  --prefix=/ \
  -platform macx-clang \
  -device-option QMAKE_APPLE_DEVICE_ARCHS=arm64 \
  -device-option "QMAKE_MACOSX_DEPLOYMENT_TARGET=$macos_target" \
  -release \
  -opensource -confirm-license \
  -gui \
  -opengl \
  -widgets \
  -no-gif \
  -no-icu \
  -no-pch \
  -no-angle \
  -no-dbus \
  -no-harfbuzz \
  -skip multimedia \
  -skip qtcanvas3d \
  -skip qtcharts \
  -skip qtconnectivity \
  -skip qtgamepad \
  -skip qtlocation \
  -skip qtmultimedia \
  -skip qtnetworkauth \
  -skip qtpurchasing \
  -skip qtremoteobjects \
  -skip qtscript \
  -skip qtsensors \
  -skip qtserialbus \
  -skip qtserialport \
  -skip qtwebchannel \
  -skip qtwebengine \
  -skip qtwebsockets \
  -nomake examples \
  -nomake tests \
  -make tools
make "-j$nproc" -k > qt.log 2>&1
make install "-j$nproc" INSTALL_ROOT="$here/install/qt-$version-macosx$macos_target-arm64"
cd ..

# Create the final tarball containing universal binaries.
tar cJf "qt-$version-macosx$macos_target-arm64.tar.xz" -C "$here/install" "qt-$version-macosx$macos_target-arm64"
