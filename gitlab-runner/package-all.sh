#!/bin/sh

set -e

readonly git_url='https://gitlab.com/gitlab-org/gitlab-runner.git'
readonly git_commit='bbcb5aba76d966f60c76629301d0e2cac4ee09b7' # v15.3.0
readonly version='15.3.0-host-check-script'

readonly build_platforms='
  linux/amd64
  linux/arm64
  darwin/amd64
  darwin/arm64
  windows/amd64
  windows/arm64
'

readonly package_types='
  deb
  rpm
'

readonly package_archs='
  amd64
  arm64
'

apt-get update
apt-get install -y --no-install-recommends \
  bzip2 \
  libffi-dev \
  rename \
  rpm \
  ruby \
  ruby-dev \
  ruby-ffi
gem install --no-document \
  fpm:1.10.2

git clone "$git_url" gitlab-runner/src
cd gitlab-runner/src
git checkout "$git_commit"
git config user.name 'user'
git config user.email 'user@localhost'
git am ../*.patch

make deps

for p in $build_platforms; do
  make runner-bin BUILD_PLATFORMS="-osarch=$p"
done

mkdir -p out/helper-images
for t in $package_types; do
  for a in $package_archs; do
    make "package-$t-arch" ARCH="$a" PACKAGE_ARCH="$a"
  done
done

cd ../..
mkdir -p out

readonly tmp=gitlab-runner/src/out
readonly out=out/gitlab-runner-$version
cp -a $tmp/binaries/gitlab-runner-darwin-amd64        $out-darwin-amd64
cp -a $tmp/binaries/gitlab-runner-darwin-arm64        $out-darwin-arm64
cp -a $tmp/binaries/gitlab-runner-windows-amd64.exe   $out-windows-amd64.exe
cp -a $tmp/binaries/gitlab-runner-windows-arm64.exe   $out-windows-arm64.exe
cp -a $tmp/deb/gitlab-runner_amd64.deb                $out-amd64.deb
cp -a $tmp/deb/gitlab-runner_arm64.deb                $out-arm64.deb
cp -a $tmp/rpm/gitlab-runner_amd64.rpm                $out-amd64.rpm
cp -a $tmp/rpm/gitlab-runner_arm64.rpm                $out-arm64.rpm
